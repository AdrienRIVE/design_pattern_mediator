#include "Mediator.h"

Mediator::Mediator()
{

}

Mediator::~Mediator()
{
    for(int i = 0 ; i < m_tab_persos.size() ; i++)
    {
        delete m_tab_persos[i];
    }
    for(int i = 0 ; i < m_tab_monstres.size() ; i++)
    {
        delete m_tab_monstres[i];
    }
}

bool Mediator::nouvelleAction()
{
    std::string action = "";
    int length = 0, i = 0, j = 0;
    std::cout << "creerMonstre(nom) / creerPerso(nom) / monstreAttaquePerso(i, j) / persoAttaqueMonstre(i,j) / persoAttaquePerso(i,j) :\n";
    std::cin >> action;
    length = action.length();

    if(action == "finir" || action == "\n" || action == "0")
    {
        return false;
    }
    else if(action.substr(0,12) == "creerMonstre")
    {
        nouveauMonstre(action.substr(13,length-14));
        return true;
    }
    else if(action.substr(0,10) == "creerPerso")
    {
        nouveauPerso(action.substr(11,length-12));
        return true;
    }
    else if(action.substr(0,19) == "monstreAttaquePerso")
    {
        i= atoi(action.substr(20,1).c_str());
        j= atoi(action.substr(22,1).c_str());
        attaque(m_tab_monstres[i], m_tab_persos[j]);
        return true;
    }
    else if(action.substr(0,19) == "persoAttaqueMonstre")
    {
        i= atoi(action.substr(20,1).c_str());
        j= atoi(action.substr(22,1).c_str());
        attaque(m_tab_persos[i], m_tab_monstres[j]);
        return true;
    }
    else if(action.substr(0,17) == "persoAttaquePerso")
    {
        i= atoi(action.substr(18,1).c_str());
        j= atoi(action.substr(20,1).c_str());
        attaque(m_tab_persos[i], m_tab_persos[j]);
        return true;
    }
    else
        return true;
}

void Mediator::nouveauPerso(std::string nom, int vie)
{
    m_tab_persos.push_back(new Personnage(nom, vie));
}

void Mediator::nouveauMonstre(std::string nom, int vie, int force, typeMonstre type)
{
    m_tab_monstres.push_back(new Monstre(nom, vie, force, type));
}

void Mediator::attaque(Personnage *attaquant, Personnage *cible)
{
    int degats = attaquant->getArme()->getDegats();
    cible->setVie(cible->getVie() - degats);
    std::cout << attaquant->getNom() << " enleve " << degats << " points de vie a " << cible->getNom() << std::endl;
    if(cible->meurt())
       std::cout << cible->getNom() << " meurt." << std::endl;
}

void Mediator::attaque(Personnage *attaquant, Monstre *cible)
{
    int degats = attaquant->getArme()->getDegats();
    cible->setVie(cible->getVie() - degats);
    std::cout << attaquant->getNom() << " enleve " << degats << " points de vie a " << cible->getNom() << std::endl;
    if(cible->meurt())
       std::cout << cible->getNom() << " meurt." << std::endl;
}

void Mediator::attaque(Monstre *attaquant, Personnage *cible)
{
    int degats = attaquant->getForce();
    cible->setVie(cible->getVie() - degats);
    std::cout << attaquant->getNom() << " enleve " << degats << " points de vie a " << cible->getNom() << std::endl;
    if(cible->meurt())
       std::cout << cible->getNom() << " meurt." << std::endl;
}



Personnage* Mediator::getPerso(int id)
{
    return m_tab_persos[id];
}

Monstre* Mediator::getMonstre(int id)
{
    return m_tab_monstres[id];
}
