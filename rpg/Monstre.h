#ifndef MONSTRE_H_INCLUDED
#define MONSTRE_H_INCLUDED

#include <string>
#include <iostream>

enum typeMonstre {deubbaze, troll, nain, sorcier};

class Monstre
{
public:
    Monstre(std::string nom = "Deubbaze", int vie = 50, int force = 10, typeMonstre type = deubbaze);

    int getForce();
    int getVie();
    std::string getNom();
    void setVie(int);

    bool meurt();
    void affiche();

protected:
    std::string m_nom;
    int m_vie_actuelle;
    int m_vie_max;
    int m_force;
    typeMonstre m_type;
};

#endif // MONSTRE_H_INCLUDED
