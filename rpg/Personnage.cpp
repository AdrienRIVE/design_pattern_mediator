#include "Personnage.h"

Personnage::Personnage(): m_vie_max(100), m_vie_actuelle(100), m_nom("Raymond Deubbaze"), m_force(20)
{
    m_arme = new Arme();
    affiche();

}

Personnage::Personnage(std::string nom, int vie) : m_vie_max(vie), m_vie_actuelle(vie), m_nom(nom), m_force(20)
{
    m_arme = new Arme();
    affiche();
}

Personnage::~Personnage()
{
    delete m_arme;
}

void Personnage::boitPotion(int pdv)
{
    setVie(m_vie_actuelle+pdv);
    std::cout << m_nom << " boit une potion : ses PV valent maintenant " << m_vie_actuelle << "." << std::endl;
}

void Personnage::affiche()
{

    std::cout << m_nom << " : " << m_vie_actuelle << "/" << m_vie_max << " de points de vie. Son arme inflige " << m_arme->getDegats() << " points de degats." << std::endl;
}

bool Personnage::meurt()
{
    return m_vie_actuelle <= 0;
}

int Personnage::getVie()
{
    return m_vie_actuelle;
}

int Personnage::getForce()
{
    return m_force;
}

std::string Personnage::getNom()
{
    return m_nom;
}

Arme* Personnage::getArme()
{
    return m_arme;
}

void Personnage::setVie(int nouvelle_vie)
{
    if(nouvelle_vie >= 0 && nouvelle_vie <= m_vie_max)
        m_vie_actuelle = nouvelle_vie;
    else if(nouvelle_vie > m_vie_max)
        m_vie_actuelle = m_vie_max;
    else
        m_vie_actuelle = 0;
}






