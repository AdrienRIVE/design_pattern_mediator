#ifndef MEDIATOR_H_INCLUDED
#define MEDIATOR_H_INCLUDED

#include "Personnage.h"
#include "Monstre.h"
#include <vector>
#include <cstdlib>

class Mediator
{
    public:
        Mediator();
        ~Mediator();
        bool nouvelleAction();
        void nouveauPerso(std::string nom = "Deubbaze", int vie=100);
        void nouveauMonstre(std::string nom = "Deubbaze", int vie = 50, int force = 10, typeMonstre type = deubbaze);
        void attaque(Personnage*, Personnage*);
        void attaque(Personnage*, Monstre*);
        void attaque(Monstre*, Personnage*);
        Personnage* getPerso(int);
        Monstre* getMonstre(int);

    private:
        std::vector<Personnage*> m_tab_persos;
        std::vector<Monstre*> m_tab_monstres;
};




#endif // MEDIATOR_H_INCLUDED
