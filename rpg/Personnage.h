#ifndef PERSONNAGE_H_INCLUDED
#define PERSONNAGE_H_INCLUDED

#include <string>
#include <iostream>

#include "Arme.h"


class Personnage
{
    public :
        Personnage();
        Personnage(std::string, int vie = 100);
        ~Personnage();
        void boitPotion(int);
        void affiche();
        bool meurt();


        //Accesseur
        int getVie();
        int getForce();
        std::string getNom();
        Arme* getArme();

        void setVie(int);

    protected :
        std::string m_nom;
        int m_vie_actuelle;
        int m_vie_max;
        int m_force;
        Arme *m_arme;

};

#endif // PERSONNAGE_H_INCLUDED
