#include "Magicien.h"

Magicien::Magicien() : Personnage(), m_mana(100), m_pouvoirSort(20)
{

}

int Magicien::getMana()
{
    return m_mana;
}
int Magicien::getPvr()
{
    return m_pouvoirSort;
}

void Magicien::setMana(int mana)
{
    m_mana = mana;
}
