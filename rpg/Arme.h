#ifndef ARME_H_INCLUDED
#define ARME_H_INCLUDED

#include <iostream>

class Arme
{
public:
    Arme(int degats = 50);
    Arme(std::string nom, int degats = 50);

    int getDegats();
    std::string getNom();
    void setNom(std::string);

protected:
    int m_degats;
    std::string m_nom;

};

#endif // ARME_H_INCLUDED
