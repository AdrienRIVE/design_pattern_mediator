#include "Monstre.h"

Monstre::Monstre(std::string nom,int vie, int force, typeMonstre type) : m_nom(nom), m_vie_actuelle(vie), m_vie_max(vie), m_force(force), m_type(type)
{
    affiche();
}

int Monstre::getForce()
{
    return m_force;
}

int Monstre::getVie()
{
    return m_vie_actuelle;
}

std::string Monstre::getNom()
{
    return m_nom;
}

void Monstre::setVie(int nouvelle_vie)
{
    if(nouvelle_vie >= 0 && nouvelle_vie <= m_vie_max)
        m_vie_actuelle = nouvelle_vie;
    else if(nouvelle_vie > m_vie_max)
        m_vie_actuelle = m_vie_max;
    else
        m_vie_actuelle = 0;
}

bool Monstre::meurt()
{
    return m_vie_actuelle <= 0;
}

void Monstre::affiche()
{
    std::cout << "Le monstre " << m_nom << " a ete cree : " << m_vie_actuelle << "/" << m_vie_max << " de points de vie, " << m_force << " points de force." << std::endl;
}
