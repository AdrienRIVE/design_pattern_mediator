#ifndef MAGICIEN_H_INCLUDED
#define MAGICIEN_H_INCLUDED

#include "Personnage.h"

class Magicien : public Personnage
{
    public:
        Magicien();

        int getMana();
        int getPvr();
        void setMana(int);


    protected:
        int m_mana;
        int m_pouvoirSort;

};

#endif // MAGICIEN_H_INCLUDED
